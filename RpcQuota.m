//
//  RpcQuota.m
//  rpcquota
//
//  Created by Jan Hacker on 04.02.09.
//  Copyright 2009 ETH Zurich. All rights reserved.
//

// this app may not try to rely on prefs writing ... to not crash in over-quota situations
// too lazy to write error handlers

#include <unistd.h>
#import "RpcQuota.h"

@implementation RpcQuota
- (id)init {
	[super init];
	quotaUsage = malloc(333);
	return self;
}

- (void)checkQuota:(id)sender {
	quotaUsage = getNFSQuotaForCurrentUser();
	//NSLog(@"a=%d  // b = %d  //  c = %d // d = %d // e = %d", 
	//	  quotaUsage[0],quotaUsage[1],quotaUsage[2],quotaUsage[3],quotaUsage[4]);
}

- (int)setTimer:(int)seconds {
	if (timer == nil) {
		NSLog(@"Enabling quota scan every %d seconds.", seconds);
		timer = [[NSTimer scheduledTimerWithTimeInterval:seconds 
												  target:self 
												selector:@selector(checkQuota:) 
												userInfo:nil 
												 repeats:YES] retain];
		return 1;
	} else {
		NSLog(@"Disabling quota timed scans.");
		[timer invalidate];
		[timer release];
		timer = nil;
		return 0;
	}
}

- (int*)quotaUsage:(id)sender {
	return quotaUsage;
}

//dealloc()
@end

/////////// more or less p(l)ain C

static int* getnfsquota(char *hostp, char *path, uid_t uid, struct dqblk *dqp)
{
	struct getquota_args gq_args;
	struct getquota_rslt gq_rslt;
	struct rquota *rquota;
	extern char *strchr();
	int	rpc_err;
	int	vflag = 1;	
	int	*usage;
	usage = malloc(32); // FIXME
	
	gq_args.gqa_pathp = path;
	gq_args.gqa_uid = uid;
	rpc_err = callaurpc(hostp, RQUOTAPROG, RQUOTAVERS,
						(vflag? RQUOTAPROC_GETQUOTA: RQUOTAPROC_GETACTIVEQUOTA),
						xdr_getquota_args, &gq_args, xdr_getquota_rslt, &gq_rslt);
	
	usage[0] = rpc_err;
	usage[4] = 0;
	if (rpc_err != RPC_SUCCESS) {
		//printf("no success\n");		
		return (usage);
	}
	//printf("getnfsquota(): rpc_err = %d\n", rpc_err);
	if (gq_rslt.status = Q_OK) {
		rquota = &gq_rslt.getquota_rslt_u.gqr_rquota;
		usage[1] = rquota->rq_curblocks/HACKBLOCKSIZE;		
		usage[2] = rquota->rq_bsoftlimit/HACKBLOCKSIZE;
		usage[3] = rquota->rq_bhardlimit/HACKBLOCKSIZE;
	}
	/*for (int i=0; i<10; i++) {
		printf("debug: usage[i] %d = %d\n", i, usage[i]);
	}*/
	return usage;
}


#define HDLEN 500

int* getNFSQuotaForCurrentUser() {
	int *qusage; // 0: RPC error,  1,2,3: rpc vals  4: 0 if no home, 1 if home found
	int verbose = 0;
	int numMounts = 0;
	int i;
	int homeFound = 0;
	struct statfs *mntinfo;
	//struct rpcent *re;
	char* host;
	char* directory;
	NSString *homeDirectory;
	NSString *userName;
	NSString *tmpStr;
	char* homedir;
	char* username;
	char* tmp;
	host  = malloc(HDLEN);
	homedir  = malloc(HDLEN);
	username = malloc(HDLEN);
	tmp = malloc(HDLEN);
	qusage = malloc(sizeof(int)*10); // FIXME
	for (i=0; i<10; i++) {
		qusage[i] = -1;
	}
	
	numMounts = getmntinfo(&mntinfo, 0);
	if (verbose) printf("%d mounts\n", numMounts);
	
	//Get Homedir, copy to C char*
	//NSLog(@"homedir: %@", [homeDirectory  lastPathComponent]);
	homeDirectory = [[NSString alloc] initWithString:NSHomeDirectory()];
	[homeDirectory getCString:homedir maxLength:HDLEN encoding:NSASCIIStringEncoding];
	// initWithBytesNoCopy:length:encoding:freeWhenDone:
	//userName = NSUserName();
	userName = [[NSString alloc] initWithString:NSUserName()];
	[userName getCString:username maxLength:HDLEN encoding:NSASCIIStringEncoding];
	
	// search mounted dirs for homedir of user
	for (i=0; i<numMounts; i++) {
		if (verbose) printf("%6s mounted from: '%15s' to '%s'\n",
							mntinfo[i].f_fstypename,
							mntinfo[i].f_mntfromname,
							mntinfo[i].f_mntonname);	
		
		if (strcmp(mntinfo[i].f_fstypename,"nfs") == 0) {
			directory = mntinfo[i].f_mntfromname;
			host = strsep(&directory,":");
			if (verbose) {
				printf("nfs mount: %s\n", mntinfo[i].f_mntfromname);
				printf("++++ host: '%s'\n", host);
				printf("++++ directory: '%s'\n", directory);
			}
			
			tmpStr = [[NSString alloc]  initWithCString:mntinfo[i].f_mntonname];
			[[tmpStr lastPathComponent] getCString:tmp maxLength:HDLEN encoding:NSASCIIStringEncoding];
			
			if (	directory && mntinfo[i].f_mntonname &&
				strcmp(homedir, directory) == 0 ||
				strcmp(homedir, mntinfo[i].f_mntonname) == 0 ||
				strcmp(tmp, username) == 0 ) {
				//printf("Hit your homedir! :-)\n");
				homeFound++;
				break;
			} // the one and only homedir, hopefully
			
			[tmpStr dealloc];
		} // nfs
	}

	if (homeFound==1) {
		qusage = getnfsquota(host, directory, getuid(), NULL);
		qusage[4] = 1;
	} else {
		NSLog(@"No NFS Homedirectory found for current user. Not enabling timer for future scans.");
		//quotaUsage[4] = 0;
		qusage[4] = 0;
	}
	
	free(homedir);
	free(host);
	free(tmp);
	free(username);
	return qusage;
}	

///////////////////////////////////////////////////////////////////////////////////
/// STOLEN from opensolaris quota.c //////////////////////////////////////////////

int
callaurpc(char *host, int prognum, int versnum, int procnum,
		  xdrproc_t inproc, char *in, xdrproc_t outproc, char *out)
{
	static enum clnt_stat clnt_stat;
	struct timeval tottimeout = {20, 0};
	
	static CLIENT *cl = NULL;
	static int oldprognum, oldversnum;
	static char oldhost[MAXHOSTNAMELEN+1];
	
	if (cl == NULL || oldprognum != prognum || oldversnum != versnum ||
	    strcmp(oldhost, host) != 0) {
		if (cl) {
			clnt_destroy(cl);
			cl = NULL;
		}
		cl = clnt_create(host, prognum, versnum, "udp"); //, &tottimeout);
		if (cl == NULL) {
			return ((int)RPC_TIMEDOUT);
		}
		if ((cl->cl_auth = authunix_create_default()) == NULL) {
			clnt_destroy(cl);
			return (RPC_CANTSEND);
		}
		oldprognum = prognum;
		oldversnum = versnum;
		(void) strcpy(oldhost, host);
		clnt_stat = RPC_SUCCESS;
	}
	
	if (clnt_stat != RPC_SUCCESS)
		return ((int)clnt_stat);	/* don't bother retrying */
	
	clnt_stat = clnt_call(cl, procnum, inproc, in,
						  outproc, out, tottimeout);
	
	return ((int)clnt_stat);
}

///////////////////////////////////////////////////////////////////////////////////
/// STOLEN from cpan's Quota.pm / .xs /////////////////////////////////////////////

struct xdr_discrim gq_des[2] = {
{ (int)Q_OK, (xdrproc_t)xdr_rquota },
{ 0, NULL }
};

bool_t
xdr_getquota_args(xdrs, gqp)
XDR *xdrs;
struct getquota_args *gqp;
{
	return (xdr_string(xdrs, &gqp->gqa_pathp, 1024) &&
			xdr_int(xdrs, &gqp->gqa_uid));
}

bool_t
xdr_getquota_rslt(xdrs, gqp)
XDR *xdrs;
struct getquota_rslt *gqp;
{
	return (xdr_union(xdrs,
					  (int *) &gqp->status, (char *) &gqp->getquota_rslt_u,
					  //(int *) &gqp->GQR_STATUS, (char *) &gqp->GQR_RQUOTA,
					  gq_des, (xdrproc_t) xdr_void));
}

bool_t
xdr_rquota(xdrs, rqp)
XDR *xdrs;
struct rquota *rqp;
{
	return (xdr_int(xdrs, &rqp->rq_bsize) &&
			xdr_bool(xdrs, &rqp->rq_active) &&
			xdr_u_long(xdrs, (unsigned long *)&rqp->rq_bhardlimit) &&
			xdr_u_long(xdrs, (unsigned long *)&rqp->rq_bsoftlimit) &&
			xdr_u_long(xdrs, (unsigned long *)&rqp->rq_curblocks) &&
			xdr_u_long(xdrs, (unsigned long *)&rqp->rq_fhardlimit) &&
			xdr_u_long(xdrs, (unsigned long *)&rqp->rq_fsoftlimit) &&
			xdr_u_long(xdrs, (unsigned long *)&rqp->rq_curfiles) &&
			xdr_u_long(xdrs, (unsigned long *)&rqp->rq_btimeleft) &&
			xdr_u_long(xdrs, (unsigned long *)&rqp->rq_ftimeleft) );
}
