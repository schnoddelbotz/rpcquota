//
//  main.m
//  rpcquota
//
//  Created by Jan Hacker on 04.02.09.
//  Copyright ETH Zurich 2009. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "TrayMenu.h"

int main(int argc, char *argv[])
{
    //return NSApplicationMain(argc,  (const char **) argv);
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    [NSApplication sharedApplication];
	
    TrayMenu *menu = [[TrayMenu alloc] init];
    [NSApp setDelegate:menu];
    [NSApp run];
	//[NSApp hide:nil];
	
    [pool release];
    return EXIT_SUCCESS;
}
