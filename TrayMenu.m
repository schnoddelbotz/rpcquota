//
//  TrayMenu.m
//  rpcquota
//
//  Created by Jan Hacker on 04.02.09.
//  Copyright 2009 ETH Zurich. All rights reserved.
//

// make it work for local fs quota as well?
// who gives himself quota? ;-)

#include <netdb.h>
#import "TrayMenu.h"
#import "RpcQuota.h"

@implementation TrayMenu

- (IBAction)setCheckInterval:(id)sender {
	if ([rq setTimer:3600]) {
		[repeatMenuItem setState:1];
	} else {
		[repeatMenuItem setState:0];
	}
}

// runs the rpc check and updates the menu
- (int) runCheck:(id)sender {
	int spaceUsage;
	
	if (!rq) {		
		rq = [[RpcQuota alloc] init];
		[rq retain];
	}
	[rq checkQuota:self];
	/*NSLog(@"aresult0: %d", [rq quotaUsage:self][0]);
	NSLog(@"aresult1: %d", [rq quotaUsage:self][1]);
	NSLog(@"aresult2: %d", [rq quotaUsage:self][2]);
	NSLog(@"aresult3: %d", [rq quotaUsage:self][3]);
	NSLog(@"aresult4: %d", [rq quotaUsage:self][4]);*/
	// 0: RPC error,  1,2,3: rpc vals  4: 0 if no home, 1 if home found
	int a,b;
	a =  [rq quotaUsage:self][1];
	b =  [rq quotaUsage:self][2];
	if ([rq quotaUsage:self][0] == 0 && [rq quotaUsage:self][4]) {
			// if rpc_err=0 and homedir found
			spaceUsage = (float)a/b * 100;	
			// rpc success
			if (spaceUsage<=90) {
				[statusItem setImage:[NSImage imageNamed:@"q-ok.png"]];
			} else if (spaceUsage>90 && spaceUsage < 95) {
				[statusItem setImage:[NSImage imageNamed:@"q-warn.png"]];
			} else {
				[statusItem setImage:[NSImage imageNamed:@"q-bad.png"]];
			}
			
			[statusItem setToolTip:[[NSString alloc] initWithFormat:@"Homedirectory Quota Usage: %d%%", spaceUsage]];
			[statusMenuItem setTitle:[[NSString alloc] initWithFormat:@"Usage: %d of %d MB (%d %%)", a,b, spaceUsage]];
		
	} else {
		[statusItem setImage:[NSImage imageNamed:@"q-none.png"]];
		[statusItem setToolTip:@"No quotas/home available/found."];		
		[statusMenuItem setTitle:@"No Quota / NFS Home Mount"];
		return 0;
	} 
	
	//[menu update];
	return 1;
}

- (void) actionQuit:(id)sender {
	[NSApp terminate:sender];
}

- (NSMenu *) createMenu {
	NSZone *menuZone = [NSMenu menuZone];
	menu = [[NSMenu allocWithZone:menuZone] init];
	NSMenuItem *menuItem; // anon menu items
	
	// quota status line in menu, using instance var
	statusMenuItem =  [menu addItemWithTitle:@"Checking quotas..." action:nil keyEquivalent:@""];
	[statusMenuItem setTarget:self];	

	// separator
	[menu addItem:[NSMenuItem separatorItem]];

	// 
	menuItem = [menu addItemWithTitle:@"Check now"
							   action:@selector(runCheck:)
						keyEquivalent:@""];
	[menuItem setTarget:self];

	//
	repeatMenuItem = [menu addItemWithTitle:@"Check every hour"
							   action:@selector(setCheckInterval:)
						keyEquivalent:@""];
	[repeatMenuItem setTarget:self];
	[repeatMenuItem setState:0];
	
	[menu addItem:[NSMenuItem separatorItem]];
	
	// Add Quit Action
	menuItem = [menu addItemWithTitle:@"Quit"
							   action:@selector(actionQuit:)
						keyEquivalent:@""];
	[menuItem setToolTip:@"Click to Quit this App"];
	[menuItem setTarget:self];
	
	return menu;
}

- (void) applicationDidFinishLaunching:(NSNotification *)notification {
	menu = [[self createMenu] retain];
	
	statusItem = [[[NSStatusBar systemStatusBar]
					statusItemWithLength:NSSquareStatusItemLength] retain];
	[statusItem setMenu:menu];
	[statusItem setHighlightMode:YES];
	[statusItem setToolTip:@"NFS/RPC Homedirectory Quota-Usage Checker"];
	[statusItem setImage:[NSImage imageNamed:@"q-init"]];


	if ([self runCheck:nil]) {
		[rq setTimer:10];
		[repeatMenuItem setState:1];
	}
}
@end
