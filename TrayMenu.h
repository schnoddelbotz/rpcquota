//
//  TrayMenu.h
//  rpcquota
//
//  Created by Jan Hacker on 04.02.09.
//  Copyright 2009 ETH Zurich. All rights reserved.
//

#import <Cocoa/Cocoa.h>
@class RpcQuota;

@interface TrayMenu : NSObject {
	@private NSStatusItem *statusItem;
	//PreferenceController *preferenceController;
	RpcQuota *rq; // object issuing RPC queries and providing state
	NSMenu *menu;
	NSMenuItem *statusMenuItem;
	NSString *statusMenuItemText;
	NSStatusBar *menuBar;
	int checkInterval;
	NSMenuItem *repeatMenuItem;
}
- (IBAction)setCheckInterval:(id)sender;
- (int)runCheck:(id)sender;
@end
