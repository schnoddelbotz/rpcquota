//
//  RpcQuota.h
//  rpcquota
//
//  Created by Jan Hacker on 04.02.09.
//  Copyright 2009 ETH Zurich. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#include <sys/param.h>
#include <sys/ucred.h>
#include <sys/mount.h>
#include <sys/quota.h>
#include <rpc/rpc.h>
#include <rpcsvc/rquota.h>

#define HACKBLOCKSIZE 2048

int* getNFSQuotaForCurrentUser();

@interface RpcQuota : NSObject {
	int *quotaUsage; // array of int containing server respone
	NSString *userMessage;
	NSTimer *timer;
}
- (IBAction)checkQuota:(id)sender; // to pass the GUI click to our C func
- (int*)quotaUsage:(id)sender; 
- (int)setTimer:(int)seconds;
@end

int
callaurpc(char *host, int prognum, int versnum, int procnum,
		  xdrproc_t inproc, char *in, xdrproc_t outproc, char *out);